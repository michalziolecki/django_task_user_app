# dockerfile for server django app

# base img
FROM python:3.6

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
RUN mkdir /django_task
WORKDIR /django_task

# install dependecies
COPY requirements /django_task
RUN pip install -r requirements

# copy project repository
COPY UserProject/ /django_task/UserProject/
# COPY files/ /files/
RUN ls -la /django_task/UserProject/*


# "Running django server"
RUN python /django_task/UserProject/manage.py migrate
ENTRYPOINT python /django_task/UserProject/manage.py runserver 0.0.0.0:8000

