# Django Task #

Exercise for MiloSolutions - user_app

### Requirements ###
* Python3.6
* Unoccupied port 8000

### Prepare virtualenv (Linux) ###

* Prepare directory for virtual env (on the root of project):
	`mkdir venv`
* Prepare virtual env module:
	`sudo apt-get install python3-venv`
* Create venv:
	`python3.6 -m venv ./venv/`
* Checkout to venv:
	`source ./venv/bin/activate`
* Install requirements:
	`pip install -r requirements`
* Check requirements:
	`pip freeze`
* Default python 3.6 if You want:
	`alias python=/usr/bin/python3.6`

### Run application ###

* Source the virtaul enviroment:
	`source ./vevn/bin/activate`
* Install requirements (if aren't installed):
	`pip install -r requirements`
* Migrate:
	`python3.6 manage.py migrate`
* Run django application:
	`python3.6 manage.py runserver`

### Run tests ###

* Command to run all test (from directory with manage.py):
	`python manage.py test --pattern="test_*.py"`

### Run with Docker ###

* Build image (run from directory with Dockerfile)
	`docker build -t django_task .`
* Run container:
	`docker run -d -p 8000:8000 django_task`
* Check running process by:
	`docker container ps`
* Stop container:
	`docker stop container <sha_id>`
