from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from django.http import HttpResponse
from .templatetags.user_extras import user_status, buzz_fizz_tag
import csv
from .models import User
from .forms import UserCreateForm


class UserCreateView(CreateView):
    model = User
    form_class = UserCreateForm


class UserDetailView(DetailView):
    model = User
    success_url = reverse_lazy('user-detail')


class UserListView(ListView):
    model = User
    fields = ['username', 'birthday']


class UserUpdateView(UpdateView):
    model = User
    fields = ['username', 'birthday']


class UserDeleteView(DeleteView):
    model = User
    success_url = reverse_lazy('user-list')


def download_users_list(request):
    users_queryset = User.objects.all()
    for user in users_queryset:
        user.bizz_fuzz = buzz_fizz_tag(user.random)
        user.eligible = user_status(user.birthday)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_list.csv"'

    file_writer = csv.writer(response, delimiter=',')
    file_writer.writerow(['Username', 'Birthday', 'Eligible', 'Random Number', 'BizzFuzz'])

    for user in users_queryset:
        file_writer.writerow([user.username, user.birthday, user.eligible, user.random, user.bizz_fuzz])

    return response
