from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from .utils import random_number


class User(AbstractUser):
    birthday = models.DateField()
    random = models.IntegerField(default=random_number)

    def get_absolute_url(self):
        return reverse('user-detail', kwargs={'pk': self.id})

    def __str__(self):
        return self.username
