from unittest import TestCase
from unittest import mock
from unittest.mock import MagicMock
from datetime import date
from ..utils import random_number, calculate_age, create_buzz_fizz
from ..templatetags.user_extras import user_status


class TestUtils(TestCase):

    @mock.patch('Users.utils.get_today')
    def test_calculate_age(self, today_mock: MagicMock):
        # given
        today_mock.return_value = date(2020, 5, 24)
        young = date(2010, 9, 8)
        adult = date(1992, 9, 10)

        # when
        age_young = calculate_age(young)
        age_adult = calculate_age(adult)

        # then
        self.assertEqual(9, age_young)
        self.assertEqual(27, age_adult)

    @mock.patch('Users.utils.get_today')
    def test_user_status(self, today_mock: MagicMock):
        # given
        today_mock.return_value = date(2020, 5, 24)
        young = date(2010, 9, 8)
        adult = date(1992, 9, 10)

        # when
        status_young = user_status(young)
        status_adult = user_status(adult)

        # then
        self.assertEqual('blocked', status_young)
        self.assertEqual('allowed', status_adult)

    def test_random_number(self):
        # given
        min_val = 1
        max_val = 100

        # when
        result = random_number(min_val, max_val)

        # then
        self.assertTrue(1 <= result <= 100)

    def test_create_buzz_fizz(self):
        # given
        fizzbuzz = 15
        expected_fizzbuzz = 'FizzBuzz'
        fizz = 9
        expected_fizz = 'Fizz'
        buzz = 10
        expected_buzz = 'Buzz'
        number = 8
        expected_number = '8'

        # when
        result1 = create_buzz_fizz(fizzbuzz)
        result2 = create_buzz_fizz(fizz)
        result3 = create_buzz_fizz(buzz)
        result4 = create_buzz_fizz(number)

        # then
        self.assertEqual(expected_fizzbuzz, result1)
        self.assertEqual(expected_fizz, result2)
        self.assertEqual(expected_buzz, result3)
        self.assertEqual(expected_number, result4)
