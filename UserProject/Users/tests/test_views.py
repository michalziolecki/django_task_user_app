from django.test import TestCase
from django.urls import reverse
from datetime import date
from ..models import User


class TestViews(TestCase):

    def setUp(self):
        User.objects.create(username="michael", birthday=date(1992, 9, 10))
        User.objects.create(username="tommy", birthday=date(1993, 10, 10))

    def test_home(self):
        # when
        response = self.client.get(reverse('home'))

        # then
        self.assertEquals(200, response.status_code)

    def test_user_list(self):
        # when
        response = self.client.get(reverse('user-list'))

        # then
        self.assertEquals(200, response.status_code)
        self.assertEquals(2, len(response.context['object_list']))

    def test_user_details(self):
        # given
        username = 'michael'
        user = User.objects.get(username=username)

        # when
        response = self.client.get(reverse('user-detail', kwargs={'pk': user.id}))

        # then
        self.assertEquals(200, response.status_code)
        self.assertEquals(username, response.context['object'].username)

    def test_user_create(self):
        # when
        response = self.client.get(reverse('user-create'))

        # then
        self.assertEquals(200, response.status_code)

    def test_user_update(self):
        # given
        user = User.objects.get(username='michael')

        # when
        response = self.client.get(reverse('user-update', kwargs={'pk': user.id}))

        # then
        self.assertEquals(200, response.status_code)

    def test_user_delete(self):
        # given
        user = User.objects.get(username='tommy')

        # when
        response = self.client.get(reverse('user-delete', kwargs={'pk': user.id}))

        # then
        self.assertEquals(200, response.status_code)
