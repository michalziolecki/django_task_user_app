from django.test import TestCase
from ..models import User
from datetime import date


class TestUserModel(TestCase):
    def setUp(self):
        User.objects.create(username="michael", birthday=date(1992, 9, 10))
        User.objects.create(username="tommy", birthday=date(1993, 10, 10))

    def test_random_number_added(self):
        user1 = User.objects.get(username='michael')
        user2 = User.objects.get(username='tommy')

        self.assertIsNotNone(user1.random)
        self.assertIsNotNone(user2.random)
        self.assertTrue(1 <= user1.random <= 100)
        self.assertTrue(1 <= user2.random <= 100)

    def test_birthday_correct(self):
        user1 = User.objects.get(username='michael')
        user2 = User.objects.get(username='tommy')

        self.assertIsNotNone(user1.random)
        self.assertIsNotNone(user2.random)
        self.assertEquals(date(1992, 9, 10), user1.birthday)
        self.assertEquals(date(1993, 10, 10), user2.birthday)
