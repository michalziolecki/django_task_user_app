from django import template
from ..utils import calculate_age, create_buzz_fizz
from datetime import date

register = template.Library()


@register.filter()
def user_status(birthday: date) -> str:
    status = "blocked"
    if calculate_age(birthday) > 13:
        status = "allowed"
    return status


@register.filter()
def buzz_fizz_tag(number: int) -> str:
    return create_buzz_fizz(number)
