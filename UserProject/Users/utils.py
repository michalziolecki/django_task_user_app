from random import randint
from datetime import date


def random_number(lower_limit=1, upper_limit=100) -> int:
    return randint(lower_limit, upper_limit)


def get_today() -> date:
    return date.today()


def calculate_age(birthday: date) -> int:
    age = 0
    today = get_today()
    this_year_birthday = date(today.year, birthday.month, birthday.day)
    if birthday > this_year_birthday:
        age = today.year - birthday.year
    else:
        age = today.year - birthday.year - 1
    return age


def create_buzz_fizz(number) -> str:
    output = ''
    if number % 5 == 0 and number % 3 == 0:
        output = 'FizzBuzz'
    elif number % 3 == 0:
        output = 'Fizz'
    elif number % 5 == 0:
        output = 'Buzz'
    else:
        output = f'{number}'
    return output
