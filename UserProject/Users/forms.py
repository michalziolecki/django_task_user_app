from django.forms import ModelForm
from .models import User
from django.forms.widgets import DateInput


class UserCreateForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'birthday']
        widgets = {
            'birthday': DateInput(attrs={'type': 'date'})
        }
        help_texts = {
            'username': None,
        }
