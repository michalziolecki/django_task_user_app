from django.urls import path
from Users.views import UserListView, UserCreateView, UserDetailView, UserUpdateView, UserDeleteView, \
    download_users_list

urlpatterns = [
    path('create/', UserCreateView.as_view(), name='user-create'),
    path('list/', UserListView.as_view(), name='user-list'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user-detail'),
    path('update/<int:pk>/', UserUpdateView.as_view(), name='user-update'),
    path('delete/<int:pk>/', UserDeleteView.as_view(), name='user-delete'),
    path('users-csv/', download_users_list, name='users-csv')
]
